from fastapi import Request, status
from fastapi.responses import JSONResponse
from asyncio import get_running_loop
from motor.motor_asyncio import AsyncIOMotorClient
from pymongo.errors import DuplicateKeyError

from config import config


async def init_db(app):
    app.mongo_client = AsyncIOMotorClient(config.mongodb_url, io_loop=get_running_loop(), uuidRepresentation="standard")

    @app.exception_handler(DuplicateKeyError)
    async def duplicate_key_exception_handler(request: Request, exc: DuplicateKeyError):
        return JSONResponse(status_code=status.HTTP_403_FORBIDDEN, content={"detail": "Duplicate key"})


async def close_db(app):
    app.mongo_client.close()
