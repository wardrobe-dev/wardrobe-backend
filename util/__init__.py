from fastapi import status
from fastapi.security import OAuth2PasswordBearer
from fastapi.exceptions import HTTPException
from jose import JWTError, jwt
from passlib.context import CryptContext
from datetime import datetime, timedelta
from flatten_dict import flatten, unflatten

from config import config

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

timedelta_units = ["days", "seconds", "microseconds", "milliseconds", "minutes", "hours", "weeks"]


def hash_password(pwd) -> str:
    return pwd_context.hash(pwd)


def verify_password(plain_pwd, hashed_pwd) -> bool:
    return pwd_context.verify(plain_pwd, hashed_pwd)


def encode_jwt(data: dict):
    payload = data.copy()
    datetime_now = datetime.utcnow()
    payload.update({"iat": datetime_now, "exp": datetime_now + timedelta(days=config.token_exp)})
    return jwt.encode(data, config.token_secret, algorithm=config.token_algo)


def decode_jwt(token: str):
    token_exception = HTTPException(
        status_code=status.HTTP_400_BAD_REQUEST, detail="Invalid token", headers={"WWW-Authenticate": "Bearer"}
    )
    try:
        payload = jwt.decode(token, config.token_secret)
        if not payload or not payload.get("sub"):
            raise token_exception
        return payload
    except JWTError:
        raise token_exception


def flatten_doc(doc):
    return flatten(doc, reducer=lambda k1, k2: k2 if not k1 else f"{k1}.{k2}")


def unflatten_doc(doc):
    return unflatten(doc, splitter=lambda flat_key: flat_key.split("."))
