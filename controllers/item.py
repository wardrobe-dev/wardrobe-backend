from typing import Optional
from uuid import uuid4, UUID
from datetime import datetime, MINYEAR
from pymongo import ASCENDING, DESCENDING

from util import flatten_doc


class ItemController:
    def __init__(self, mongo_coll):
        self.mongo_coll = mongo_coll

    async def create_one(self, new_item: dict) -> dict:
        new_item.update({"_id": uuid4(), "created_at": datetime.utcnow()})
        insert_result = await self.mongo_coll.insert_one(new_item)
        return {"success": insert_result.acknowledged, "item_id": insert_result.inserted_id}

    async def find_one(self, item_id: UUID) -> dict:
        return await self.mongo_coll.find_one({"_id": item_id})

    async def find_many(
        self, query_filter: dict, order: Optional[str], limit: Optional[int], start_at: Optional[datetime]
    ) -> list:
        if order == "ascending":
            sort_order = ASCENDING
            query_filter["created_at"] = {"$gt": start_at if start_at else datetime(year=MINYEAR, month=1, day=1)}
        else:
            sort_order = DESCENDING
            query_filter["created_at"] = {"$lt": start_at if start_at else datetime.utcnow()}

        item_cursor = self.mongo_coll.find(flatten_doc(query_filter)).sort([("created_at", sort_order)]).limit(limit)
        return await item_cursor.to_list(limit)

    async def update_one(self, item_id: UUID, new_item: dict) -> dict:
        new_item.pop("id", None)  # remove the id field
        new_item["updated_at"] = datetime.utcnow()
        update_result = await self.mongo_coll.update_one({"_id": item_id}, {"$set": flatten_doc(new_item)})
        return {"success": update_result.acknowledged, "modified_count": update_result.modified_count}

    async def delete_one(self, item_id: UUID) -> dict:
        delete_result = await self.mongo_coll.delete_one({"_id": item_id})
        return {"success": delete_result.acknowledged, "deleted_count": delete_result.deleted_count}

    async def delete_many(self, item_ids: list) -> dict:
        delete_result = await self.mongo_coll.delete_many({"_id": {"$in": item_ids}})
        return {"success": delete_result.acknowledged, "deleted_count": delete_result.deleted_count}

    async def count(self, query_filter: dict) -> int:
        return await self.mongo_coll.count_documents(flatten_doc(query_filter))
