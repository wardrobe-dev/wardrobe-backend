from fastapi.encoders import jsonable_encoder
from enum import Enum
from typing import Any, Tuple
from uuid import uuid4, UUID
from datetime import datetime
from pymongo import ReturnDocument

from models.user import ResUser, TokenData
from util import hash_password, verify_password, encode_jwt, flatten_doc


class LoginStatus(str, Enum):
    INVALID_EMAIL = "invalid email"
    INACTIVE_ACCOUNT = "inactive account"
    INVALID_PASSWORD = "invalid password"


class UserController:
    def __init__(self, mongo_coll):
        self.mongo_coll = mongo_coll

    async def create_one(self, new_user: dict) -> dict:
        new_user.update({
            "_id": uuid4(),
            "password": hash_password(new_user.get("password")),
            "is_verified": False,  # avoid users making themselves verified
            "is_superuser": False,  # avoid users making themselves superuser
            "created_at": datetime.utcnow(),
        })
        insert_result = await self.mongo_coll.insert_one(new_user)
        return {"success": insert_result.acknowledged, "user_id": insert_result.inserted_id}

    async def find_one(self, user_id: UUID) -> dict:
        return await self.mongo_coll.find_one({"_id": user_id})

    async def update_one(self, user_id: UUID, new_user: dict) -> dict:
        new_user.pop("id", None)  # remove the id field
        if "password" in new_user.keys():
            new_user["password"] = hash_password(new_user.get("password"))
        new_user["updated_at"] = datetime.utcnow()

        update_result = await self.mongo_coll.update_one({"_id": user_id}, {"$set": flatten_doc(new_user)})
        return {"success": update_result.acknowledged, "modified_count": update_result.modified_count}

    async def delete_one(self, user_id: UUID) -> dict:
        delete_result = await self.mongo_coll.delete_one({"_id": user_id})
        return {"success": delete_result.acknowledged, "deleted_count": delete_result.deleted_count}

    async def count(self, query_filter: dict) -> int:
        return await self.mongo_coll.count_documents(flatten_doc(query_filter))

    async def count_all(self) -> int:
        return await self.mongo_coll.estimated_document_count()

    async def login(self, email: str, password: str) -> Tuple[str, Any]:
        req_user = await self.mongo_coll.find_one({"email": email})

        if not req_user:
            return LoginStatus.INVALID_EMAIL, None
        if not req_user["is_active"]:
            return LoginStatus.INACTIVE_ACCOUNT, None
        if not verify_password(password, req_user["password"]):
            return LoginStatus.INVALID_PASSWORD, None

        updated_user = await self.mongo_coll.find_one_and_update(
            {"email": email}, {"$set": {"last_login": datetime.utcnow()}}, return_document=ReturnDocument.AFTER
        )

        # filter out unnecessary fields and convert the fields to json compatible types
        res_user = ResUser(**updated_user).dict()
        token_data = jsonable_encoder(TokenData(**res_user), by_alias=False)

        return encode_jwt(token_data), res_user
