from typing import Optional
from pydantic import BaseSettings, Field


class GlobalConfig(BaseSettings):
    """Global configurations."""

    # This variable will be loaded from the .env file. However, if there is a
    # shell environment variable having the same name, that will take precedence.

    # Define global variables with the Field class
    # Environment specific variables do not need the Field class

    # environment config
    env_state: Optional[str] = Field("dev", env="ENV_STATE")

    # server config
    server_host: Optional[str] = Field("127.0.0.1", env="SERVER_HOST")
    server_port: Optional[int] = Field(8000, env="SERVER_PORT")
    server_ssl_cert: Optional[str] = Field("", env="SERVER_SSL_CERT")
    server_ssl_key: Optional[str] = Field("", env="SERVER_SSL_KEY")

    # database config
    mongodb_url: Optional[str] = Field("mongodb://localhost:27017", env="MONGODB_URL")
    mongodb_user_db: Optional[str] = Field("db-user", env="MONGODB_USER_DB")
    mongodb_item_db: Optional[str] = Field("db-item", env="MONGODB_ITEM_DB")
    mongodb_user_coll: Optional[str] = Field("user", env="MONGODB_USER_COLL")
    mongodb_item_coll: Optional[str] = Field("item", env="MONGODB_ITEM_COLL")
    mongodb_use_ssl: Optional[bool] = Field(False, env="MONGODB_USE_SSL")

    # token config
    token_secret: Optional[str] = Field("", env="TOKEN_SECRET")
    token_algo: Optional[str] = Field("HS256", env="TOKEN_ALGO")
    token_exp: Optional[int] = Field(365, env="TOKEN_EXP")

    class Config:
        """Loads the dotenv file."""

        env_file: str = ".env"
        env_file_encoding: str = "utf-8"


class DevConfig(GlobalConfig):
    """Development configurations."""

    class Config:
        env_prefix: str = "DEV_"


class ProdConfig(GlobalConfig):
    """Production configurations."""

    class Config:
        env_prefix: str = "PROD_"


class FactoryConfig:
    """Returns a config instance depending on the ENV_STATE variable."""

    def __init__(self, env_state: Optional[str]):
        self.env_state = env_state

    def __call__(self):
        if self.env_state == "dev":
            return DevConfig()

        elif self.env_state == "prod":
            return ProdConfig()


config = FactoryConfig(GlobalConfig().env_state)()

if __name__ == "__main__":
    print(config.__repr__())
