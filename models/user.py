from fastapi.security import HTTPBasicCredentials, OAuth2PasswordRequestForm
from typing import Optional
from pydantic import BaseModel, Field, UUID4, EmailStr
from datetime import date, datetime

# password must contain at least 1 letter and 1 number, and at least 6-digit long
_pwd_regex = "^(?=.*\d)(?=.*[a-zA-Z]).{6,}$"


class PersonalData(BaseModel):
    gender: Optional[str] = Field(None)  # male, female, other
    birthdate: Optional[date] = Field(None)
    height: Optional[float] = Field(None)  # cm
    weight: Optional[float] = Field(None)  # kg
    nationality: Optional[str] = Field(None)
    ethnicity: Optional[str] = Field(None)


"""
class FullUser(BaseModel):  # this class is for reference only and not exposed
    id: UUID4 = Field(..., alias="_id")
    email: EmailStr = Field(...)
    password: str = Field(..., min_length=6, max_length=54, regex=_pwd_regex)
    first_name: str = Field(...)
    last_name: str = Field(...)
    personal_data: PersonalData = Field({})
    item_count: int = Field(0)
    is_active: bool = Field(True)  # only admin or program can update this field
    is_verified: bool = Field(False)  # only admin or program can update this field
    is_superuser: bool = Field(False)  # only admin can update this field
    created_at: datetime = Field(...)
    updated_at: datetime = Field(...)
    last_login: datetime = Field(...)
    metadata: dict = Field(...)  # reserve for future use, only admin or program can create this field
"""


class BaseUser(BaseModel):
    id: Optional[UUID4] = Field(None, alias="_id")
    first_name: Optional[str] = Field(None)
    last_name: Optional[str] = Field(None)
    email: Optional[EmailStr] = Field(None)
    personal_data: Optional[PersonalData] = Field({})
    item_count: int = Field(0)
    is_active: Optional[bool] = Field(True)  # only admin or program can update this field
    is_verified: Optional[bool] = Field(False)  # only admin or program can update this field
    is_superuser: Optional[bool] = Field(False)  # only admin can update this field


class CreateUser(BaseUser):
    email: EmailStr = Field(...)  # email is required when creating user
    password: str = Field(..., min_length=6, max_length=54, regex=_pwd_regex)  # password is required when creating user


class UpdateUser(BaseUser):
    password: Optional[str] = Field(
        None, min_length=6, max_length=54, regex=_pwd_regex
    )  # optional but when it has value, follow the constraints


class FilterUser(BaseUser):
    pass


class ResUser(BaseUser):
    created_at: Optional[datetime] = Field(None)
    updated_at: Optional[datetime] = Field(None)
    last_login: Optional[datetime] = Field(None)


class LoginCredentials(HTTPBasicCredentials):
    username: EmailStr = Field(...)
    password: str = Field(..., min_length=6, max_length=54, regex=_pwd_regex)


class OAuth2Credentials(LoginCredentials, OAuth2PasswordRequestForm):
    pass


class Token(BaseModel):
    access_token: str = Field(...)
    token_type: str = Field(...)


class TokenData(BaseModel):
    sub: UUID4 = Field(..., alias="id")
    email: EmailStr = Field(...)
    is_active: bool = Field(...)
    is_verified: bool = Field(...)
    is_superuser: bool = Field(...)
