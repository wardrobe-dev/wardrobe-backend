from typing import Optional, List
from pydantic import BaseModel, Field, UUID4, AnyUrl
from datetime import datetime


class Picture(BaseModel):
    name: Optional[str] = Field(None)
    type: Optional[str] = Field(None)  # jpeg, png, webp
    path: Optional[AnyUrl] = Field(None)


"""
class FullItem(BaseModel):  # this class is for reference only and not exposed
    id: UUID4 = Field(..., alias="_id")
    user_id: UUID4 = Field(...)
    name: str = Field(...)
    quantity: int = Field(1)
    size: str = Field(...)
    type: str = Field(...)  # women, men, kids, baby
    brand: str = Field(...)
    category: str = Field(...)  # tops, bottoms, outerwear, innerwear, accessory, shoes
    subcategory: str = Field(...)  # shirt, pants, etc
    functionality: str = Field(...)  # casual, formal, sports
    cover_picture: Picture = Field(...)
    pictures: List[Picture] = Field(..., min_items=1, max_items=6)
    description: str = Field(..., max_length=500)
    tags: List[str] = Field(...)
    created_at: datetime = Field(...)
    updated_at: datetime = Field(...)
    metadata: dict = Field(...)  # reserve for future use, only admin or program can update this field
"""


class BaseItem(BaseModel):
    id: Optional[UUID4] = Field(None, alias="_id")
    user_id: Optional[UUID4] = Field(None)  # only admin or program can update this field
    name: Optional[str] = Field(None)
    quantity: Optional[int] = Field(1)
    size: Optional[str] = Field(None)
    type: Optional[str] = Field(None)  # women, men, kids, baby
    brand: Optional[str] = Field(None)
    category: Optional[str] = Field(None)  # tops, bottoms, outerwear, innerwear, accessory, shoes
    subcategory: Optional[str] = Field(None)  # shirt, pants, etc
    functionality: Optional[str] = Field(None)  # casual, formal, sports
    cover_picture: Optional[Picture] = Field({})
    pictures: Optional[List[Picture]] = Field([], min_items=1, max_items=6)
    description: Optional[str] = Field(None, max_length=500)
    tags: Optional[List[str]] = Field([])


class CreateItem(BaseItem):
    user_id: UUID4 = Field(...)
    name: str = Field(...)
    cover_picture: Picture = Field(...)
    pictures: List[Picture] = Field(..., min_items=1, max_items=6)


class UpdateItem(BaseItem):
    pass


class FilterItem(BaseItem):
    pass


class ResItem(BaseItem):
    created_at: Optional[datetime] = Field(None)
    updated_at: Optional[datetime] = Field(None)
