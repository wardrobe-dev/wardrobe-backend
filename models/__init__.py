"""
pydantic Field behavior:

1. Model fields with alias accept only the alias as valid input, not the original field name.
And FastAPI uses the alias for output field name. It implicitly calls Model.dict(by_alias=True).
Setting by_alias=True makes the Model use alias as output field name instead of the original one.
That is, FastAPI uses the alias as valid input field name, as well as generating the http response.
It auto-converts the field name to its alias. This behavior makes alias the public name of the field.

2. Model.dict(exclude_unset=True) will remove fields that are not in the request body.
Even if the field is given a default value, it will be excluded in the converted dict.
But as long as it exists in request body, it won't be excluded even when the value is None or empty string.
This is useful when updating only certain fields, instead of replacing the entire instance.
Therefore when using exclude_unset=True, setting default value for optional fields becomes useless.

3. Default value of a field will remain the same for the model, because it's a class variable.
Therefore when using uuid() or datetime.now() as default value, it won't generate a new unique value
when the model is called next time.

4. Optional fields with None as default value can still have validation constraints.
It can still be absent from the request body, but when the field value is not empty
it will be validated by the constraints.

5. To control the output, either define a separate response model, or make use of the following param:
response_model_include={set of field name}
response_model_exclude{set of field name}
response_model_by_alias=True/False
response_model_exclude_unset=True/False
response_model_exclude_defaults=True/False
response_model_exclude_none=True/False

6. To control the input, define a separate input model, and make use of the following param
by_alias=True/False
skip_defaults=True/False
exclude_unset=True/False
exclude_defaults=True/False
exclude_none=True/False
"""
