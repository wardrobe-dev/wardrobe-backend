from fastapi import APIRouter, Path, Query, Body, Depends
from typing import Optional, List
from pydantic import UUID4
from datetime import datetime

from models.item import CreateItem, UpdateItem, FilterItem, ResItem
from routers.exceptions import permission_exception, item_exception
from controllers import ItemController
from util import oauth2_scheme, decode_jwt
from config import config

_item_database = f"{config.env_state}-{config.mongodb_item_db}"
_item_collection = config.mongodb_item_coll


async def init_item_routes(app):
    item_coll = app.mongo_client[_item_database][_item_collection]
    index_info = await item_coll.index_information()
    if "user_id_1" not in index_info.keys():
        item_coll.create_index("user_id", sparse=True, background=True)

    item_controller = ItemController(item_coll)
    router = APIRouter(
        prefix="/item",
        tags=["item"],
        dependencies=[Depends(oauth2_scheme)],
        # responses={status.HTTP_404_NOT_FOUND: {"description": "Not found"}},
    )

    # Dependencies
    async def validate_token(token: str = Depends(oauth2_scheme)):
        return decode_jwt(token)

    async def authorize_create_item(item: CreateItem = Body(...), token_payload: dict = Depends(validate_token)):
        if item.user_id != UUID4(token_payload["sub"]) and not token_payload["is_superuser"]:
            raise permission_exception
        return item

    async def authorize_own_item(item_id: UUID4 = Path(...), token_payload: dict = Depends(validate_token)):
        req_item = await item_controller.find_one(item_id)
        if not req_item:
            raise item_exception
        if req_item["user_id"] != UUID4(token_payload["sub"]) and not token_payload["is_superuser"]:
            raise permission_exception
        return req_item

    async def authorize_filter_item(item_filter: FilterItem = Body(...), token_payload: dict = Depends(validate_token)):
        query_filter = item_filter.dict(by_alias=True, exclude_unset=True)
        if "_id" in query_filter.keys():
            req_item = await item_controller.find_one(query_filter["_id"])  # users can only search their own items
            if req_item and req_item["user_id"] != UUID4(token_payload["sub"]) and not token_payload["is_superuser"]:
                raise permission_exception
        if (
            "user_id" in query_filter.keys()
            and query_filter["user_id"] != UUID4(token_payload["sub"])  # users cannot search other users' items
            and not token_payload["is_superuser"]  # unless they are superuser
        ):
            raise permission_exception
        query_filter.pop("cover_picture", None)
        query_filter.pop("pictures", None)
        query_filter.pop("description", None)  # TODO: search by description
        query_filter.pop("tags", None)  # TODO: search by tag
        return query_filter

    async def authorize_update_item(
        item_id: UUID4 = Path(...), item: UpdateItem = Body(...), token_payload: dict = Depends(validate_token)
    ):
        req_item = await item_controller.find_one(item_id)
        new_item = item.dict(exclude_unset=True)
        if not req_item:
            raise item_exception
        if (
            req_item["user_id"] != UUID4(token_payload["sub"])  # users can only update their own items
            or "user_id" in new_item.keys()
            and new_item["user_id"] != UUID4(token_payload["sub"])  # users cannot change their items to others
        ) and not token_payload["is_superuser"]:  # unless they are superuser
            raise permission_exception
        return item_id, item

    async def authorize_delete_items(
        ids: List[UUID4] = Body(..., embed=True), token_payload: dict = Depends(validate_token)
    ):
        # item_cursor = item_coll.find({"_id": {"$in": ids}}).limit(len(ids))
        # req_items = [item async for item in item_cursor]
        req_items = await item_controller.find_many({"_id": {"$in": ids}}, None, len(ids), None)
        if (
            any(req_item["user_id"] != UUID4(token_payload["sub"]) for req_item in req_items)
            and not token_payload["is_superuser"]
        ):
            raise permission_exception
        return ids

    # Routers
    @router.post("/")
    async def create_item(item: CreateItem = Depends(authorize_create_item)):
        return await item_controller.create_one(item.dict(by_alias=True))  # convert id to its alias _id

    @router.get("/{item_id}", response_model=ResItem, response_model_by_alias=False)  # use id instead of its alias _id
    async def get_item(req_item: dict = Depends(authorize_own_item)):
        return req_item

    @router.post("/find", response_model=List[ResItem], response_model_by_alias=False)
    async def get_items_by_filter(
        query_filter: dict = Depends(authorize_filter_item),
        order: Optional[str] = Query("descending"),
        limit: Optional[int] = Query(20),
        start_at: Optional[datetime] = Query(None),
    ):
        return await item_controller.find_many(query_filter, order, limit, start_at)

    @router.post("/count")
    async def get_item_count(query_filter: dict = Depends(authorize_filter_item)):
        return await item_controller.count(query_filter)

    @router.put("/{item_id}", dependencies=[Depends(authorize_update_item)])
    async def update_item(item_id: UUID4 = Path(...), item: UpdateItem = Body(...)):
        new_item = item.dict(exclude_unset=True)  # exclude fields that aren't in the request body
        return await item_controller.update_one(item_id, new_item)

    @router.delete("/{item_id}", dependencies=[Depends(authorize_own_item)])
    async def delete_item(item_id: UUID4 = Path(...)):
        return await item_controller.delete_one(item_id)

    @router.post("/delete")
    async def delete_items(item_ids: List[UUID4] = Depends(authorize_delete_items)):
        return await item_controller.delete_many(item_ids)

    app.include_router(router)
