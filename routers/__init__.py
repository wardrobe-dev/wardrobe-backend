from fastapi import Request
from time import time
from routers.user import init_user_routes
from routers.item import init_item_routes


async def init_routes(app):
    @app.middleware("http")
    async def add_process_time_header(request: Request, call_next):
        start_time = time()
        response = await call_next(request)
        process_time = time() - start_time
        response.headers["X-Process-Time"] = str(process_time)
        return response

    """
    The router dependencies are executed first,
    then the dependencies in the decorator,
    and then the normal parameter dependencies.
    """

    await init_user_routes(app)
    await init_item_routes(app)
