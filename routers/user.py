from fastapi import APIRouter, Path, Body, Depends
from pydantic import UUID4

from models.user import CreateUser, UpdateUser, FilterUser, ResUser, LoginCredentials, OAuth2Credentials, Token
from routers.exceptions import (
    permission_exception,
    user_exception,
    email_exception,
    account_exception,
    password_exception,
)
from controllers import LoginStatus, UserController
from util import oauth2_scheme, decode_jwt
from config import config

_user_database = f"{config.env_state}-{config.mongodb_user_db}"
_user_collection = config.mongodb_user_coll


async def init_user_routes(app):
    user_coll = app.mongo_client[_user_database][_user_collection]
    index_info = await user_coll.index_information()
    if "email_1" not in index_info.keys():
        user_coll.create_index("email", unique=True)

    user_controller = UserController(user_coll)
    router = APIRouter(prefix="/user", tags=["user"])

    # Dependencies
    async def validate_token(token: str = Depends(oauth2_scheme)):
        return decode_jwt(token)

    async def authorize_get_user(user_id: UUID4 = Path(...), token_payload: dict = Depends(validate_token)):
        if user_id != UUID4(token_payload["sub"]) and not token_payload["is_superuser"]:
            raise permission_exception
        return user_id

    async def authorize_update_user(
        user_id: UUID4 = Path(...), user: UpdateUser = Body(...), token_payload: dict = Depends(validate_token)
    ):
        new_user = user.dict(exclude_unset=True)
        if (
            user_id != UUID4(token_payload["sub"])
            or any(key in ["is_active", "is_verified", "is_superuser"] for key in new_user.keys())
        ) and not token_payload["is_superuser"]:
            raise permission_exception
        return user_id, user

    async def authorize_superuser_operation(token_payload: dict = Depends(validate_token)):
        if not token_payload["is_superuser"]:
            raise permission_exception
        return token_payload

    async def validate_login(token):
        if token == LoginStatus.INVALID_EMAIL:
            raise email_exception
        elif token == LoginStatus.INACTIVE_ACCOUNT:
            raise account_exception
        elif token == LoginStatus.INVALID_PASSWORD:
            raise password_exception

    # Routers
    @router.post("/")
    async def create_user(user: CreateUser = Body(...)):
        return await user_controller.create_one(user.dict(by_alias=True))  # convert id to its alias _id

    @router.get("/{user_id}", response_model=ResUser, response_model_by_alias=False)  # use id instead of its alias _id
    async def get_user(user_id: UUID4 = Depends(authorize_get_user)):
        req_user = await user_controller.find_one(user_id)
        if not req_user:
            raise user_exception
        return req_user

    # TODO: add endpoint for searching users with pagination

    @router.post("/count", dependencies=[Depends(authorize_superuser_operation)])
    async def get_user_count(user_filter: FilterUser = Body(...)):
        return await user_controller.count(user_filter.dict(by_alias=True, exclude_unset=True))

    @router.get("/count/all", dependencies=[Depends(authorize_superuser_operation)])
    async def get_user_count_all():
        return await user_controller.count_all()

    @router.put("/{user_id}", dependencies=[Depends(authorize_update_user)])
    async def update_user(user_id: UUID4 = Path(...), user: UpdateUser = Body(...)):
        new_user = user.dict(exclude_unset=True)  # exclude fields that aren't in the request body
        return await user_controller.update_one(user_id, new_user)

    @router.delete("/{user_id}", dependencies=[Depends(authorize_superuser_operation)])
    async def delete_user(user_id: UUID4 = Path(...)):
        return await user_controller.delete_one(user_id)

    @app.get("/login", response_model=Token, tags=["auth"])
    async def login_oauth2(form_data: OAuth2Credentials = Depends()):
        # this is shorthand to (form_data: OAuth2Credentials = Depends(OAuth2Credentials))
        signed_jwt, res_user = await user_controller.login(form_data.username, form_data.password)
        await validate_login(signed_jwt)
        return {"access_token": signed_jwt, "token_type": "bearer"}

    @app.post("/login", tags=["auth"])
    async def login_credentials(credentials: LoginCredentials = Body(...)):
        signed_jwt, res_user = await user_controller.login(credentials.username, credentials.password)
        await validate_login(signed_jwt)
        return {"login_success": bool(signed_jwt), "jwt": signed_jwt}

    app.include_router(router)
