from fastapi import status
from fastapi.exceptions import HTTPException

# common exceptions
permission_exception = HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="No permission")

# item exceptions
item_exception = HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Item not found")

# user exceptions
user_exception = HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
email_exception = HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Incorrect email")
account_exception = HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Inactive account")
password_exception = HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="Incorrect password")
